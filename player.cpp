#include <ncurses.h>
#include <fstream>
#include <vector>
using namespace std;

/*This class stores file data*/
class fileData{
	public:
	string strFileName;
	string strFileInfo;
	bool isDirectory;
	bool isSelected;
	bool isText;
	bool isImage;
};

class pwd{
	public:
	string strPWD="";

	pwd(){
		system("pwd > /tmp/viewfilescurses");
		ifstream fileList("/tmp/viewfilescurses");
		if(!fileList.is_open()){
			throw runtime_error("Searching for pwd\n/tmp/viewfilescurses not found.\n");
		}
		getline(fileList,strPWD,'\n');
	}

	void backDirectory(){
		if(strPWD[strPWD.length()-1]=='/' && strPWD.length()>1)
			strPWD.pop_back();
		for(int i=strPWD.length()-1;i>=0 && strPWD[i]!='/';i--)
			strPWD.pop_back();
	}

	void forwardDirectory(fileData directory){
		strPWD+=directory.strFileName;
	}

	string getLS(){
		return "ls " + strPWD + " > /tmp/viewfilescurses";
	}

	string getLSinfo(){
		return "ls -lsh " + strPWD + " > /tmp/viewfilescurses";
	}
	
};




void readFiles(vector<fileData>& files, pwd workDir){
	files.clear();

	//This code sets up the ls list used for changing directories
	system(workDir.getLS().c_str());
	ifstream fileList("/tmp/viewfilescurses");
	
	//This code sets up the info list that is displayed
	system(workDir.getLSinfo().c_str());
	ifstream fileInfoList("/tmp/viewfilescurses");

	//Exits if file isn't found
	if(!fileList.is_open()){
		throw runtime_error("Running ls\n/tmp/viewfilescurses not found.\n");
	}

	string input;

	//This reads in the files
	while(fileList.good()){
		fileData file;
		getline(fileList,file.strFileName,'\n');
		getline(fileInfoList,file.strFileInfo,'\n');
		files.push_back(file);
	}
}
/*
void formatFiles(vector<string>& files){
	system
}*/

int main()
{	
	initscr();			/* Start curses mode 		  */
	noecho();			//gets rid of text echo
	//Creates char for input
	char input=-1;
	int counter = 0;
	//SelectedFileIndex
	int selected=0;
	pwd workDir = pwd();//This creates a present work directory object
	do{


		//Clear screen
		erase();
		vector<fileData> files;
		

		//Detect Input
		if(input=='j'){
			workDir.backDirectory();
			selected=0;
		}
		if(input=='k')
			selected++;
		if(input=='l')
			selected--;

		//Start by displaying present work directory
		printw(workDir.getLS().c_str());
		printw("\n");
		//This loads the files list
		readFiles(files,workDir);

		//
		if(input==';'){
			workDir.forwardDirectory(files[selected]);
			selected=0;
		}

			
		for(int i=0;i<files.size();i++){
			if(i==selected){
				attron(A_REVERSE);
				printw(files[i].strFileName.c_str());
			}
			else{
				attroff(A_REVERSE);
				printw(files[i].strFileName.c_str());
			}
			printw("\n");	
		}


		refresh();/* Print it on to the real screen */
	}while((input=getch())!='q');
	endwin();			/* End curses mode		  */

	return 0;
}

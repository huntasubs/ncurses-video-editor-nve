#include <iostream>
#include <vector>
#include "utils.h"

int main(int argc, char *argv[]) {
	if (argc != 2) {
		std::cout << "not enough arguments\n";
		return 1;
	}

    std::cout << diff_time_codes(std::string(argv[1]), std::string("0:0:0"), mult99_doubles);
    return 0;
}

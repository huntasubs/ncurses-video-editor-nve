#include <iostream>
#include <vector>

#include "utils.h"


int main(int argc, char *argv[]) {
	if (argc != 3) {
		std::cout << "not enough arguments\n";
		return 1;
	}

    std::cout << diff_time_codes(std::string(argv[1]), std::string(argv[2]), subtract_doubles);
    return 0;
}

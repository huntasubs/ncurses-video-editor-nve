#include <string>
#include <vector>
#include <fstream>
#include "utils.h"

using namespace std;
//I don't like the normal implementation of split. So... a more pythonic function
//
//This should be changed to pass in by const reference, but I'm tired.
std::vector<std::string> string_split(std::string inputStr, char splitter) {
	std::vector<std::string> strings;
	for(int i=0; i<inputStr.length(); i++){
		std::string temp = "";
		for(; i < inputStr.length() && inputStr[i] != splitter; i++)
			temp += inputStr[i];
		strings.push_back(temp);
	}
	return strings;
}

bool compare(string strL, string strR){
	for(int i=0;i<strL.length();i++)
		if(strL[i]!=strR[i])
			return false;
	return true;
}

void removeTrailingZeros(string& str){
	if(str[str.length()-1]=='0' || str[str.length()-1]=='-'){
		str=str.substr(0,str.length()-1);
		removeTrailingZeros(str);
	}
	return;
}

std::string diff_time_codes(std::string l, std::string r, double(*operation)(double, double)) {
	std::vector<std::string> lvec = string_split(l, ':');
	std::vector<std::string> rvec = string_split(r, ':');

	if (lvec.size() != 3 || lvec.size() != 3) {
		return "c";
    }

    int sh1 = stoi(lvec[0]) * 3600;
    int sm1 = stoi(lvec[1]) * 60;
    double ss1 = stod(lvec[2]);

    double result1 = sh1 + sm1 + ss1;

    int sh2 = stoi(rvec[0]) * 3600;
    int sm2 = stoi(rvec[1]) * 60;
    double ss2 = stod(rvec[2]);

    double result2 = sh2 + sm2 + ss2;

    double sum = operation(result1, result2);

    int sumHours = (int) (sum / 3600.0);
    sum -= sumHours * 3600.0;
    int sumMinutes = (int) (sum / 60.0);
    sum -= sumMinutes * 60.0;
	return std::to_string(sumHours) + ":" + std::to_string(sumMinutes) + ":" + std::to_string(sum);
}

//Helper functions for diff time
double add_doubles(double d1, double d2)
{
    return d1 + d2;
}

double subtract_doubles(double d1, double d2)
{
    return d1 - d2;
}

double mult99_doubles(double d1, double d2)
{
    return d1*0.986;
}

//Writes a command and makes a executable bash script
void writeCommand(string str){
	fstream out;
	out.open("./commands",fstream::out);
	system("chmod u+x commands");
	out << str;
	out.close(); //Output Stream
}

//Writes the command history file
void writeCommandHistory(string history){
	history.insert(0,"echo ");
	system((history + " >> command_history").c_str());
}

/*for diagnostics, but writes to a test file*/
void writeTest(string str){
	fstream out;
	out.open("./test",fstream::out);
	out << str;
	out.close(); //Output Stream
}
